import {ApiProperty} from "@nestjs/swagger";
import {IsEmail, IsString, Length} from "class-validator";

export class CreateUserDto {
  @ApiProperty({ example: 'user@gmail.com', description: 'Email пользователя' })
  @IsString({ message: 'Должна быть строка в емейл' })
  @IsEmail({}, { message: 'Невалидный емейл' })
  readonly email: string;

  @ApiProperty({ example: '143fd1!', description: 'Пароль пользователя' })
  @IsString({ message: 'Должна быть строка в пароле' })
  @Length(4, 20, { message: 'Длина пароля от 4 до 20 символов' })
  readonly password: string;
}