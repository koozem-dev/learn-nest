import {ApiProperty} from "@nestjs/swagger";
import {IsNumber, IsString} from "class-validator";

export class AddRoleDto {
  @IsString({ message: 'Должно быть строкой' })
  @ApiProperty({ example: 'ADMIN', description: 'Роль пользователя' })
  readonly value: string;

  @IsNumber({}, { message: 'Должно быть числом' })
  @ApiProperty({ example: 1, description: 'ID пользователя' })
  readonly userId: number;
}