FROM node:17.9.0-alpine
LABEL authors="koozem"

WORKDIR /app
COPY package*.json /

RUN npm install

COPY . .

COPY ./dist ./dist

CMD ["npm", "run", "start:dev"]